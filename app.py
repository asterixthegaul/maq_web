from flask import Flask, render_template, request, redirect, url_for, flash, Markup, session, jsonify
from passlib.hash import pbkdf2_sha256
import smtplib, datetime
from models import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

app = Flask(__name__)
app.config["SECRET_KEY"] = "265e66e9-7c9a-4101-9b5a-be1bf641395f"
app.config["SQLALCHEMY_DATABASE_URI"] = "postgres://swlzynge:w58sIGTcJtHPzcUzkM8CtQezbJQA5hmD@horton.elephantsql.com:5432/swlzynge"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)

def is_field_empty(*fields):
	for f in fields:
		if f == "":
			return True
	
	return False

def is_email_in_use(email):
	email_count = User.query.filter_by(email=email).count()

	if email_count == 1:
		return True

	return False

def publicid_exist(public_id):
	publicid_count = User.query.filter_by(public_id=public_id).count()

	if publicid_count == 1:
		return True
	
	return False

def device_token_exists(token):
	token_count = Device.query.filter_by(token=token).count()

	if token_count == 1:
		return True
	
	return False

def is_account_activated(email):
	activated = User.query.filter_by(email=email).first().activated
	return activated

def check_password(email, password):
	data_hash = User.query.filter_by(email=email).first().password
	return pbkdf2_sha256.verify(password, data_hash)

def valid_token(token):
	token_count = Token.query.filter_by(token=token).count()

	if token_count == 1:
		return True
	
	return False

def send_email(to, verify_link):
	from_addr = "jasmin.bakalovic.ets@gmail.com"
	msg = MIMEMultipart()

	msg["From"] = from_addr
	msg["To"] = to
	msg["Subject"] = "Verify your account for air quality measure"
	body = "In order to use featuers of the air quality measure application you have to verify your account on this link: {}".format(verify_link)
	body += "\n\nIf you have any problems activating your account reply on this message." 

	msg.attach(MIMEText(body, "plain"))

	server = smtplib.SMTP("smtp.gmail.com", 587)
	server.starttls()
	server.login(from_addr, "asterix.milf")
	text = msg.as_string()
	server.sendmail(from_addr, to, text)
	server.quit()

def device_name_exists(device_name, device_names):
	if device_name in device_names:
		return True
	
	return False

def average_temperature(measurements):
	sum = 0.0
	for measurement in measurements:
		sum += measurement.temperature
	
	return round(sum / len(measurements), 2)

def average_pressure(measurements):
	sum = 0.0
	for measurement in measurements:
		sum += measurement.pressure
	
	return round(sum / len(measurements), 2)

def average_humidity(measurements):
	sum = 0.0
	for measurement in measurements:
		sum += measurement.humidity
	
	return round(sum / len(measurements), 2)

def average_pm1(measurements):
	sum = 0.0
	for measurement in measurements:
		sum += measurement.pm1
	
	return round(sum / len(measurements), 2)

def average_pm25(measurements):
	sum = 0.0
	for measurement in measurements:
		sum += measurement.pm25
	
	return round(sum / len(measurements), 2)

def average_pm10(measurements):
	sum = 0.0
	for measurement in measurements:
		sum += measurement.pm10
	
	round(sum / len(measurements), 2)

def average_measurements(measurements):
	todays_measurements = []
	date_today = str(datetime.utcnow().year)
	
	if datetime.utcnow().month < 9:
		date_today += '-0' + str(datetime.utcnow().month)
	else:
		date_today += '-' + str(datetime.utcnow().month)

	if datetime.utcnow().day < 9:
		date_today += '-0' + str(datetime.utcnow().day)
	else:
		date_today += '-' + str(datetime.utcnow().day)

	for measurement in measurements:
		for m in measurement:
			if str(m.date_time).split(' ')[0] == date_today:
				todays_measurements.append(m)

	return {
		"temperature": average_temperature(todays_measurements), 
		"pressure": average_pressure(todays_measurements),
		"humidity": average_humidity(todays_measurements),
		"pm1": average_pm1(todays_measurements),
		"pm25": average_pm25(todays_measurements),
		"pm10": average_pm25(todays_measurements)
	}

@app.route("/")
def home():
	if "signedin" in session:
		return render_template("home.html", 
			first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
			surname=User.query.filter_by(public_id=session["public_id"]).first().surname
		)
	else:
		return render_template("home.html")

@app.route("/signin", methods=["GET", "POST"])
def signin():
	if request.method == "GET":
		if "signedin" in session:
			return redirect(url_for("device"))
		else:
			return render_template("signin.html")
	else:
		email = request.form.get("email")
		password = request.form.get("password")

		if is_field_empty(email, password):
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: All fields are important!</p></div>")
			flash(message)
			return redirect(url_for('signin'))

		if is_email_in_use(email) == False:
			message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: This email does not belong to any account. Register one!</b> </p></div>")
			flash(message)
			return redirect(url_for("signup"))

		if check_password(email, password) == False:
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: Your password is incorrect!</b> </p></div>")
			flash(message)

			return redirect(url_for("signin"))

		if is_account_activated(email) == False:
			message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: Your account is not activated. Go to your email and follow details there.</b> </p></div>")
			flash(message)

			return redirect(url_for("signin"))

		session["signedin"] = True
		session["public_id"] = User.query.filter_by(email=email).first().public_id
		return redirect(url_for("device"))


@app.route("/signup", methods=["GET", "POST"])
def signup():
	if request.method == "GET":
		if "signedin" in session:
			return redirect(url_for("device"))
		else:
			return render_template("signup.html")
	else:
		public_id = uuid.uuid4()
		first_name = request.form.get("first_name")
		surname = request.form.get("surname")
		email = request.form.get("email")
		password = request.form.get("password")

		if is_field_empty(first_name, surname, email, password):
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: All fields are important!</p></div>")
			flash(message)
			return redirect(url_for("signup"))

		if is_email_in_use(email):
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: Email is in use!</p></div>")
			flash(message)
			return redirect(url_for("signup"))

		hash = pbkdf2_sha256.encrypt(password, rounds=200000, salt_size=16)
		user = User(first_name=first_name, public_id=public_id, surname=surname, email=email, password=hash)

		db.session.add(user)
		db.session.commit()

		send_email(email, "http://127.0.0.1:5000/verify_account/{}".format(public_id))

		message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info:</b> Please check your email: <a href=\"#\">{}</a> to verify your account.</p></div>".format(email))
		flash(message)

		return redirect(url_for("signup"))

@app.route("/logout")
def logout():
	if "signedin" in session:
		session.pop("signedin", None)
		session.pop("public_id", None)
		return redirect(url_for("home"))
	else:
		message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: You are not loged in!</b> </p></div>")
		flash(message)
		
		return redirect(url_for("signin"))

@app.route("/verify_account/<string:public_id>", methods=["GET", "POST"])
def verify_account(public_id):
	if publicid_exist(public_id):
		user = User.query.filter_by(public_id=public_id).first()

		if user.activated == True:
			message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: Your account is already activated!</p></div>")
			flash(message)
			return redirect(url_for("signin"))

		user.activated = True

		db.session.commit()

		message = Markup("<div class=\"uk-alert-success\" uk-alert><p><b>Info</b>: Your account is activated. You can now sign in!</p></div>")
		flash(message)
		return redirect(url_for("signin"))

	return redirect(url_for("signup"))

@app.route("/verify_device", methods=["GET", "POST"])
def verify_device():
	res = request.get_json()
	token = res["token"]

	if device_token_exists(token):
		token = Device.query.filter_by(token=token).first()

		if token.activated == True:
			return "DEVICE_ALREADY_ACTIVATED"

		token.activated = True

		db.session.commit()

		return "DEVICE_ACTIVATED"
	else:
		return "BAD_TOKEN"

@app.route("/device/data", methods=["GET", "POST"])
def store_data():
	res = request.get_json()

	token = res["token"]
	temperature = res["temperature"]
	pressure = res["pressure"]
	humidity = res["humidity"]
	pm1 = res["pm1"]
	pm25 = res["pm25"]
	pm10 = res["pm10"]

	m = Measurement(temperature=temperature, pressure=pressure, humidity=humidity, pm1=pm1, pm25=pm25, pm10=pm10, token=token)
	db.session.add(m)
	db.session.commit()

	return "DATA_STORED"

@app.route("/device", methods=["GET"])
def device():
	if request.method == "GET":
		if "signedin" not in session:
			message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: In order to access device section of the page you have to be signed in!</p></div>")
			flash(message)
			return redirect(url_for("signin"))
		else:
			devices = []
			measurements = []

			tokens = Token.query.filter_by(public_id=session["public_id"]).all()
			data_devices = Device.query.all()

			for i in range(len(tokens)):
				for j in range(len(data_devices)):
					if tokens[i].token == data_devices[j].token:
						devices.append(data_devices[j])
						measurements.append(Measurement.query.filter_by(token=tokens[i].token).all())

			return render_template("device.html", 
				first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
				surname=User.query.filter_by(public_id=session["public_id"]).first().surname,
				devices=devices, e_devices=enumerate(devices), measurements=measurements
			)

@app.route("/device/create", methods=["GET", "POST"])
def create_device():
	if request.method == "GET":
		if "signedin" not in session:
			message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: In order to access create device section of the page you have to be signed in!</p></div>")
			flash(message)
			return redirect(url_for("signin"))
		else:
			return render_template("create_device.html", 
				first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
				surname=User.query.filter_by(public_id=session["public_id"]).first().surname
			)
	else:
		device_name = request.form.get("device_name")
		timing = request.form.get("timing")
		location = request.form.get("location")

		if is_field_empty(device_name, timing, location):
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: All fields are important!</p></div>")
			flash(message)
			return redirect(url_for("create_device"))

		user = User.query.filter_by(public_id=session["public_id"]).first()
		tokens = Token.query.filter_by(public_id=user.public_id).all()
		device_names = []

		for token in tokens:
			device_token = Device.query.filter_by(token=token.token).first()
			if device_token is not None:
				device_names.append(device_token.name)

		if device_name_exists(device_name, device_names):
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: Device with given name already exists!</p></div>")
			flash(message)
			return redirect(url_for("create_device"))

		user.generate_token_for_device()
		tokens = Token.query.filter_by(public_id=user.public_id).all()
		last_token = tokens[len(tokens) - 1].token

		device = Device(name=device_name, timing=int(timing), location=location, token=last_token)
		db.session.add(device)
		db.session.commit()

		message = Markup("<div class=\"uk-alert-success\" uk-alert><p><b>Info</b>: New devices has been added!</p></div>")
		flash(message)
		return redirect(url_for("device"))

@app.route("/delete/device/<string:token>", methods=["GET"])
def delete_device(token):
	if valid_token(token) == True:
		token_in_devices = Device.query.filter_by(token=token).first()
		db.session.delete(token_in_devices)

		token_in_tokens = Token.query.filter_by(token=token).first()
		db.session.delete(token_in_tokens)

		db.session.commit()

		message = Markup("<div class=\"uk-alert-success\" uk-alert><p><b>Info</b>: Device has been removed!</p></div>")
		flash(message)
		return redirect(url_for("device"))
	else:
		message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: Invalid token!</p></div>")
		flash(message)
		return redirect(url_for("device"))

@app.route("/search", methods=["GET", "POST"])
def search():
	if request.method == "GET":
		if 'signedin' in session:
			return render_template("search.html", 
			first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
			surname=User.query.filter_by(public_id=session["public_id"]).first().surname
		)
		else:
			return render_template("search.html")
	else:
		try:
			city = request.form.get("search").split(',')[0]
			state = request.form.get("search").split(',')[1]
		except:
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: You have to provide City, State in order to search!</p></div>")
			flash(message)
			return redirect(url_for("search"))

		location = city + ',' + state
		devices = Device.query.filter_by(location=location).all()
		
		if len(devices) < 1:
			message = Markup("<div class=\"uk-alert-danger\" uk-alert><p><b>Error</b>: Given City, State does not exist!</p></div>")
			flash(message)
			if 'signedin' in session:
				return render_template("search.html", 
					first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
					surname=User.query.filter_by(public_id=session["public_id"]).first().surname
				)
			else:
				return render_template("search.html")

		measurements = []
		for device in devices:
			measurement = Measurement.query.filter_by(token=device.token).all()

			if len(measurement) > 0:
				measurements.append(measurement)

		if not len(measurements) > 0:
			message = Markup("<div class=\"uk-alert-primary\" uk-alert><p><b>Info</b>: There are no measurements for the given City, State!</p></div>")
			flash(message)
			if 'signedin' in session:
				return render_template("search.html", 
					first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
					surname=User.query.filter_by(public_id=session["public_id"]).first().surname
				)
			else:
				return render_template("search.html")

		am = average_measurements(measurements)
		if 'signedin' in session:
			return render_template("search.html", 
				first_name=User.query.filter_by(public_id=session["public_id"]).first().first_name,
				surname=User.query.filter_by(public_id=session["public_id"]).first().surname,
				average_measurements=am, location=location
			)
		else:
			return render_template("search.html", average_measurements=am, location=location)
