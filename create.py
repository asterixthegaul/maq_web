from flask import Flask
from models import *

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "postgres://swlzynge:w58sIGTcJtHPzcUzkM8CtQezbJQA5hmD@horton.elephantsql.com:5432/swlzynge"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)

def main():
	db.create_all()

if __name__ == "__main__":
	with app.app_context():
		main()
