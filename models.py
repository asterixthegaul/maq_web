from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.mysql import TIME
import uuid
from datetime import datetime

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    public_id = db.Column(db.String, unique=True, nullable=False)
    first_name = db.Column(db.String, nullable=False)
    surname = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)
    activated = db.Column(db.Boolean, default=0)
    token = db.relationship("Token", backref="public", lazy=True)

    def generate_token_for_device(self):
        uuid_token = list(str(uuid.uuid4()))
        token = ""

        for i in range(len(uuid_token)):
            if uuid_token[i] != "-":
                token += uuid_token[i]

        user_token = Token(public_id=self.public_id, token=token)
        db.session.add(user_token)
        db.session.commit()

class Token(db.Model):
    __tablename__ = "tokens"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    public_id = db.Column(db.String, db.ForeignKey("users.public_id"), nullable=False)
    token = db.Column(db.String, unique=True, nullable=False)

class Device(db.Model):
    __tablename__ = "devices"
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    timing = db.Column(db.Integer, nullable=False)
    location = db.Column(db.String, nullable=False)
    activated = db.Column(db.Boolean, nullable=False, default=0)
    token = db.Column(db.String, nullable=False)

class Measurement(db.Model):
    __tablename__ = "measurements"
    id = db.Column(db.Integer, primary_key=True)
    temperature = db.Column(db.Float, nullable=False)
    pressure = db.Column(db.Float, nullable=False)
    humidity = db.Column(db.Float, nullable=False)
    pm1 = db.Column(db.Float, nullable=False)
    pm25 = db.Column(db.Float, nullable=False)
    pm10 = db.Column(db.Float, nullable=False)
    token = db.Column(db.String, nullable=False)
    date_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())