#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <SoftwareSerial.h>

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)

typedef struct network
{
  char ssid[20];
  char password[20];
} Network;

typedef struct device
{
  char token[40];
} Device;

#define LENG 31   //0x42 + 31 bytes equal to 32 bytes
unsigned char buf[LENG];

int PM01Value=0;          //define PM1.0 value of the air detector module
int PM2_5Value=0;         //define PM2.5 value of the air detector module
int PM10Value=0;         //define PM10 value of the air detector module


Network n = {"asterix01", "636523636523"};
Device d = {"693e37c6754d415bac212903a8c7244a"};
Adafruit_BME280 bme;

bool valid_token(const char* token)
{
  HTTPClient http;

  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& JSONencoder = JSONbuffer.createObject(); 

  JSONencoder["token"] = token;
  char JSONmessageBuffer[300];
  JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

  http.begin("http://f37ba046.ngrok.io/verify_device");
  http.addHeader("Content-Type", "application/json");

  int http_code = http.POST(JSONmessageBuffer);

  bool return_status = false;
  if (http_code == 200)
  {
    String payload = http.getString(); 

    if (payload == "DEVICE_ALREADY_ACTIVATED" || payload == "DEVICE_ACTIVATED")
      return_status = true; 
  }
  
  http.end();
  return return_status;
}

char checkValue(unsigned char *thebuf, char leng)
{  
  char receiveflag=0;
  int receiveSum=0;

  for(int i=0; i<(leng-2); i++){
  receiveSum=receiveSum+thebuf[i];
  }
  receiveSum=receiveSum + 0x42;
 
  if(receiveSum == ((thebuf[leng-2]<<8)+thebuf[leng-1]))  //check the serial data 
  {
    receiveSum = 0;
    receiveflag = 1;
  }
  return receiveflag;
}

int transmitPM01(unsigned char *thebuf)
{
  int PM01Val;
  PM01Val=((thebuf[3]<<8) + thebuf[4]); //count PM1.0 value of the air detector module
  return PM01Val;
}

//transmit PM Value to PC
int transmitPM2_5(unsigned char *thebuf)
{
  int PM2_5Val;
  PM2_5Val=((thebuf[5]<<8) + thebuf[6]);//count PM2.5 value of the air detector module
  return PM2_5Val;
  }

//transmit PM Value to PC
int transmitPM10(unsigned char *thebuf)
{
  int PM10Val;
  PM10Val=((thebuf[7]<<8) + thebuf[8]); //count PM10 value of the air detector module  
  return PM10Val;
}

void send_data(char* token)
{
  HTTPClient http;

  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& JSONencoder = JSONbuffer.createObject(); 

  JSONencoder["token"] = token;
  JSONencoder["temperature"] = bme.readTemperature();
  JSONencoder["pressure"] = bme.readPressure() / 100.0F;
  JSONencoder["humidity"] = bme.readHumidity();
  JSONencoder["pm1"] = PM01Value;
  JSONencoder["pm25"] = PM2_5Value;
  JSONencoder["pm10"] = PM10Value;
  
  char JSONmessageBuffer[300];
  JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

  http.begin("http://f37ba046.ngrok.io/device/data");
  http.addHeader("Content-Type", "application/json");

  http.POST(JSONmessageBuffer);
  http.end();
}

void setup()
{
  Serial.begin(9600);
  Serial.setTimeout(1500);
  WiFi.begin(n.ssid, n.password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.printf("Connecting to: %s...\n", n.ssid); 
  }

  Serial.printf("Connected to SSID: %s\n", n.ssid);

  bool status = bme.begin(0x76);  
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    if (valid_token(d.token))
    {
      if(Serial.find(0x42)){    //start to read when detect 0x42
        Serial.readBytes(buf,LENG);
    
        if(buf[0] == 0x4d){
          if(checkValue(buf,LENG)){
            PM01Value=transmitPM01(buf); //count PM1.0 value of the air detector module
            PM2_5Value=transmitPM2_5(buf);//count PM2.5 value of the air detector module
            PM10Value=transmitPM10(buf); //count PM10 value of the air detector module 
          }           
        } 
      }
      send_data(d.token);
    }
    else
      Serial.println("Error: Token is not valid");
  }
  else
    Serial.println("Error: No WiFi connection!");

  delay(30000);
}

